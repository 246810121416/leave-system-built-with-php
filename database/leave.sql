-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 08, 2019 at 08:11 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `leave`
--

-- --------------------------------------------------------

--
-- Table structure for table `employeedepartment`
--

DROP TABLE IF EXISTS `employeedepartment`;
CREATE TABLE IF NOT EXISTS `employeedepartment` (
  `dptId` int(11) NOT NULL AUTO_INCREMENT,
  `dptName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dptId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employeedepartment`
--

INSERT INTO `employeedepartment` (`dptId`, `dptName`) VALUES
(1, 'Administration and Human Resources'),
(3, 'Agriculture and Environmental Statistics'),
(5, 'Business and Industry Statistics'),
(6, 'Communications and Public Relations'),
(7, 'District Statistics and Capacity Development'),
(8, 'Finance and Accounts'),
(9, 'Information Technology'),
(10, 'Internal Audit'),
(11, 'Legal Services and Board Affairs\r\n'),
(12, 'Macro-Economic Statistics'),
(13, 'Population and Social Statistics'),
(14, 'Procurement and Disposal Unit'),
(15, 'Socio-Economic Surveys'),
(16, 'Statistical Coordination Services'),
(17, 'Geo-Information Services');

-- --------------------------------------------------------

--
-- Table structure for table `employeedesignation`
--

DROP TABLE IF EXISTS `employeedesignation`;
CREATE TABLE IF NOT EXISTS `employeedesignation` (
  `desiId` int(11) NOT NULL AUTO_INCREMENT,
  `desiDesignationName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`desiId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employeedesignation`
--

INSERT INTO `employeedesignation` (`desiId`, `desiDesignationName`) VALUES
(1, 'Administrator'),
(5, 'Director\r\n'),
(7, 'Senior Officer'),
(8, 'Officer');

-- --------------------------------------------------------

--
-- Table structure for table `employeeinfo`
--

DROP TABLE IF EXISTS `employeeinfo`;
CREATE TABLE IF NOT EXISTS `employeeinfo` (
  `eId` int(11) NOT NULL AUTO_INCREMENT,
  `eFirstName` varchar(45) DEFAULT NULL,
  `eLastName` varchar(45) DEFAULT NULL,
  `eGender` varchar(10) DEFAULT NULL,
  `ePhoneNumberPersonal` varchar(20) DEFAULT NULL,
  `eDpt` varchar(25) DEFAULT NULL,
  `eDesignation` varchar(25) DEFAULT NULL,
  `eEmailAddress` varchar(100) DEFAULT NULL,
  `ePassword` varchar(255) DEFAULT NULL,
  `eType` varchar(25) NOT NULL,
  PRIMARY KEY (`eId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employeeinfo`
--

INSERT INTO `employeeinfo` (`eId`, `eFirstName`, `eLastName`, `eGender`, `ePhoneNumberPersonal`, `eDpt`, `eDesignation`, `eEmailAddress`, `ePassword`, `eType`) VALUES
(1, 'Deogratius', 'Mutyaba', 'Male', '999', NULL, 'Executive Director', NULL, '111', 'admin'),
(2, 'Frank', 'Wakha', 'Male', '999', NULL, 'HR', NULL, '111', 'hr');

-- --------------------------------------------------------

--
-- Table structure for table `employeeleaveapplicationdetails`
--

DROP TABLE IF EXISTS `employeeleaveapplicationdetails`;
CREATE TABLE IF NOT EXISTS `employeeleaveapplicationdetails` (
  `lId` int(11) NOT NULL AUTO_INCREMENT,
  `Section` varchar(25) NOT NULL,
  `leave_type` varchar(25) NOT NULL,
  `eid` int(11) NOT NULL,
  `lLeaveFromDate` date DEFAULT NULL,
  `lLeaveToDate` date DEFAULT NULL,
  `lTotalLeaveDays` int(11) DEFAULT NULL,
  `lTotalLeaveDaysRemain` int(11) DEFAULT NULL,
  `Status` varchar(25) NOT NULL,
  PRIMARY KEY (`lId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `leavedetails`
--

DROP TABLE IF EXISTS `leavedetails`;
CREATE TABLE IF NOT EXISTS `leavedetails` (
  `lType` varchar(30) NOT NULL,
  `lTotalDays` int(11) DEFAULT NULL,
  PRIMARY KEY (`lType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `leavedetails`
--

INSERT INTO `leavedetails` (`lType`, `lTotalDays`) VALUES
('Earn Leave', 15),
('Maternity Leave', 21),
('Study Leave', 180);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
