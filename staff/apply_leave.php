<?PHP
	session_start();
	if($_SESSION['sid'] == session_id() && $_SESSION['user'] == "employee")
	{
		$connection = @mysql_connect("localhost", "root", "") or die(mysql_error());
		$sql = "SELECT * FROM leave.leavedetails";
		$result = mysql_query($sql, $connection);
		?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apply Leave</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url(../images/bg.gif);
}
</style>
<link href="../style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../jquery.js"></script>
<script>
  function total_days()
  {
    var start_date = document.getElementById("start_date");
    var end_date = document.getElementById("end_date");
    var start_day = new Date(start_date.value);
    var end_day = new Date(end_date.value);
    var milliseconds_per_day = 1000 * 60 * 60 * 24;
  
    var millis_between = end_day.getTime() - start_day.getTime();
    var days = millis_between / milliseconds_per_day;
  
    // Round down.
    //alert( Math.floor(days));
    
    var total_days = (Math.floor(days)) + 1;
    var combined = total_days;
    //alert(combined);
    //document.getElementById("date").value = (Math.floor(days)) ;
    var days_requested = document.getElementById('days_requested');
    days_requested.value = combined;
  }
</script>
</head>

<body>
<div id="container">
<?php include 'header.php'; ?>
  <div id="content_panel">
    <div id="heading">Apply Leave<hr size="2" color="#FFFFFF" ice:repeating=""/>
</div>
    <form action="apply_leave_db.php" method="post">
        
  	<div class="leave_type" >
        <label for="leave_type" ><span>Leave Type <span class="required">*</span></span>
         	<select name="leave_type" id="leave_type">
		 	<?PHP
           		while ($row = mysql_fetch_array($result))
            	{
                	$leave_type = $row['lType'];
                	echo "<option value=\"".$leave_type."\">".$leave_type."</option>";
            	}
          	?>
          	</select>
        </label>
	</div>
          <label><span>Section <span class="required">*</span></span>
          <input type="text" name="section"  placeholder="Section" required="required"/>
        </label>
     
        <label for="start_date" ><span>Date to Commence <span class="required">*</span></span>
        	<input type="date" name="start_date" id="start_date" onchange="total_days()" />
        </label>
        
        <label for="end_date" ><span>Date to resume <span class="required">*</span></span>
        	<input type="date" name="end_date" id="end_date" onchange="total_days()" />
        </label>
    
    <div class="leave_requested_days" >     
        <label for="days_requested" ><span>Days Requested </span>
        	<input type="text" name="days_requested" id="days_requested" readonly="true" placeholder="No. of Days"/>
        </label>
   	</div>
    
    <div class="button" >
        <label>
          <input type="submit" value="Submit Request" id="submit"/>
        </label>
	</div>
    
    </form>
  </div>
<?php include 'sidebar.php'; ?>
<?php include 'footer.php'; ?>
</div>
</body>
</html>
<?php
	}
	else
	{
		header("Location: ../index.html");
	}
	mysql_close($connection);
?>
