<?PHP
	session_start();
	if($_SESSION['sid'] == session_id() && $_SESSION['user'] == "hr")
	{	
		$eId = $_SESSION['employee_id'];
		$staff_id = $_GET['staff_id'];
		$start_date = $_GET['start_date'];
		
		$connection = @mysql_connect("localhost", "root", "") or die(mysql_error());
		
		$sql1 = "SELECT * FROM leave.employeeleaveapplicationdetails WHERE eId = '".$staff_id."' AND lLeaveFromDate = '".$start_date."'";
		$sql2 = "SELECT * FROM leave.employeeinfo WHERE eId = '".$staff_id."'";
		
		$result1 = mysql_query($sql1, $connection);
		$result2 = mysql_query($sql2, $connection);
		
		$no_of_rows = mysql_num_rows($result1);
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Leave History</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url(../images/bg.gif);
}
</style>
<link href="../style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">
<?php include 'header.php';?>
  <div id="content_panel">
    <div id="heading">Leave Requests Details<hr size="2" color="#FFFFFF" ice:repeating=""/></div>
    <?PHP
		while($row = mysql_fetch_array($result1))
		{                
			$staff_id = $row['eId'];
			$leave_type = $row['leave_type'];
			$Section = $row['Section'];
            $start_date = $row['lLeaveFromDate'];
			$end_date = $row['lLeaveToDate'];
			$no_of_days = $row['lTotalLeaveDays'];
			$status = $row['Status'];
		}
		while($row1 = mysql_fetch_array($result2))
		{
			$first_name = $row1['eFirstName'];
			$last_name = $row1['eLastName'];
            $gender = $row1['eGender'];
            $phone = $row1['ePhoneNumberPersonal'];
            $dpt = $row1['eDpt'];
            $des = $row1['eDesignation'];
            $email = $row1['eEmailAddress'];
		}
	?>
    <div id="form">
    <form method="post" action="approve_reject_db.php">
    <fieldset>
    <legend>General Information</legend>
    <label for="staff_id"><span>Staff ID </span>
    	<input type="text" name="staff_id" id="staff_id" readonly="true" value="<?php echo $staff_id ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Staff Name </span>
    	<input type="text" readonly="true" value="<?php echo $first_name." ".$last_name ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Email </span>
        <input type="text" readonly="true" value="<?php echo $email ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Gender </span>
        <input type="text" readonly="true" value="<?php echo $gender ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Phone Number </span>
        <input type="text" readonly="true" value="<?php echo $phone ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Department </span>
        <input type="text" readonly="true" value="<?php echo $dpt?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Designation </span>
        <input type="text" readonly="true" value="<?php echo $des ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    </fieldset>
    <br />
    <fieldset>
    <legend>Leave Information</legend>
    <label for="leave_type"><span>Leave Type </span>
    	<input type="text" name="leave_type" id="leave_type" readonly="true" value="<?php echo $leave_type ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="leave_date"><span>Leave Date </span>
    	<input type="text" name="start_date" id="start_date" readonly="true" value="<?php echo $start_date ?>" style="background-color:#F6F6F6; color:#069" /> &ndash; <input type="text" name="end_date" id="end_date" readonly="true" value="<?php echo $end_date ?>" style="background-color:#F6F6F6; color:#069" /><input type="text" name="no_of_days" id="no_of_days" readonly="true" value="<?PHP echo $no_of_days ?> Day(s)" style="background-color:#F6F6F6; color:#069; width:80px; margin-left:10px;" />
        
    </label>
    </fieldset>
    
    <br />
    
    <fieldset>
    <legend>Approve/Reject Leave</legend>
    <label for="current_leave_status"><span>Current Status </span>
    	<input type="text" readonly="true" value="<?php echo $status ?>" style="background-color:#F6F6F6; color:#069"/>
    </label>
    <label for="approve_reject"><span>Approve / Reject </span>
    	<select name="approve_reject" id="approve_reject">
            <option value="Approved">Approve</option>
            <option value="Rejected">Reject</option>
        </select>
    </label>
    <label>
    	<input type="submit" value="Submit" />
  	 </label>
    </fieldset>
    </form>
    </div>
  </div>
<?php include 'sidebar.php'; ?>
<?php include 'footer.php'; ?>
</div>
</body>
</html>
<?php
	}
	else
	{
		header("Location: ../index.html");
	}
	mysql_close($connection);
?>
