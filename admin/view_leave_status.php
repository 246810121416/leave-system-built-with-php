<?PHP
	session_start();
	if($_SESSION['sid'] == session_id() && $_SESSION['user'] == "admin")
	{	
		$eId = $_SESSION['employee_id'];
		
		$connection = @mysql_connect("localhost", "root", "") or die(mysql_error());
		
		$sql = "SELECT * FROM leave.employeeleaveapplicationdetails";
		
		$result = mysql_query($sql, $connection);
		
		$no_of_rows = mysql_num_rows($result);
		
		if($no_of_rows == 0)
		{
			echo 	"<script>
					alert(\"No Leave Requests to Show!\");
					window.location=\"index.php\";</script>";
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Leave History</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url(../images/bg.gif);
}
</style>
<link href="../style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
<?php include 'header.php'; ?>
  <div id="content_panel">
    <div id="heading">Leave Requests Received<hr size="2" color="#FFFFFF" ice:repeating=""/></div>
    <label>
    <div id="table">
    	<span><table border="1" bgcolor="#006699" >
				<tr>
                	<th width="120px">Staff ID</th>
                	<th width="120px">Leave Type</th>
					<th width="120px">Section</th>
					<th width="120px">Start Date</th>
					<th width="120px">No. of Days</th>
					<th width="120px">End Date</th>
                    <th width="120px">Status</th>
				</tr>
			</table></span>
     <?PHP
		while($row = mysql_fetch_array($result))
		{
			@$id= $row['eId'];
			@$section = $row['Section'];
			@$leave_type = $row['leave_type'];
			@$start_date = $row['lLeaveFromDate'];
			@$end_date = $row['lLeaveToDate'];
			@$no_of_days = $row['lTotalLeaveDays'];
			@$status = $row['Status'];
			
			echo "<table border=\"1\">
					<tr>
						<td width=\"120px\">".$id."<a href='staff_profile.php?staff_id=".$id."'\> View Profile</a></td>
						<td width=\"120px\">".$leave_type."</td>
						<td width=\"120px\">".$section."</td>
						<td width=\"120px\">".$start_date."</td>
						<td width=\"120px\">".$no_of_days."</td>
						<td width=\"120px\">".$end_date."</td>
						<td width=\"120px\">".$status."</td>
					</tr>
				</table>";
		}
	?>
    </label>
  </div>
  </div>
  <?php include 'sidebar.php'; ?>
<?php include 'footer.php'; ?>
</div>
</body>
</html>
<?php
	}
	else
	{
		header("Location: ../index.html");
	}
	mysql_close($connection);
?>
