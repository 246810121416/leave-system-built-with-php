<?php
	session_start();
	if($_SESSION['sid'] == session_id() && $_SESSION['user'] == "admin")
  {
    $connection = @mysql_connect("localhost", "root", "") or die(mysql_error());
    $sql = "SELECT * FROM leave.employeedepartment";
    $sql1 = "SELECT * FROM leave.employeedesignation";
    $result = mysql_query($sql, $connection);
    $result1 = mysql_query($sql1, $connection);   	
		?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Employee</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url(../images/bg.gif);
}
</style>
<link href="../style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
<?php include 'header.php'; ?>
  <div id="content_panel">
    <div id="heading">Add Employee<hr size="2" color="#FFFFFF" ice:repeating=""/>
</div>
    <form action="add_staff_db.php" method="post">
        <label for="full_name" ><span>Name <span class="required">*</span></span>
          <input type="text" name="first_name" id="first_name" placeholder="First" required="required"/>
          <input type="text" name="last_name" id="last_name" placeholder="Last" required="required"/>     
        </label>
         <label for="email_id" ><span>Email ID <span class="required">*</span></span>
          <input type="email" name="email_id" id="email_id" placeholder="Email" required="required" style="width:560px" />
        </label>
        <label><span>Phone Number <span class="required">*</span></span>
          <input type="text" name="phone_number" id="phone_number" placeholder="Phone Number" required >
        </label>
        <label for="password" ><span>Create Password <span class="required">*</span></span>
          <input type="password" name="password" id="password" placeholder="Create Password" required="required" />
        </label>
        <label><span>Gender <span class="required">*</span></span>
          <select name="gender">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
        </label>
        <label><span>Department <span class="required">*</span></span>
          <select name="department">
            <?php
              while ($row = mysql_fetch_array($result))
              {
                  $dpt = $row['dptName'];
                  echo "<option value=\"".$dpt."\">".$dpt."</option>";
              }
            ?>
          </select>
        </label>
        <label><span>Designation <span class="required">*</span></span>
          <select name="designation">
            <?php
              while ($row = mysql_fetch_array($result1))
              {
                  $des = $row['desiDesignationName'];
                  echo "<option value=\"".$des."\">".$des."</option>";
              }
            ?>
          </select>
        </label>
        <label>
          <input type="submit" value="Add" />
        </label>
    </form>
  </div>
<?php include 'sidebar.php'; ?>
<?php include 'footer.php'; ?>
</body>
</html>
<?php
	}
	else
	{
		header("Location: ../index.html");
	}
?>
