<?PHP
  session_start();
  if($_SESSION['sid'] == session_id() && $_SESSION['user'] == "admin")
  { 
    @$staff_id = $_SESSION['employee_id'];
    @$FirstName = $_SESSION['FirstName'];
    @$LastName = $_SESSION['LastName'];
    @$Gender = $_SESSION['Gender'];
    @$PhoneNumber = $_SESSION['PhoneNumber'];
    @$Dpt = $_SESSION['Dpt'];
    @$Des = $_SESSION['Des'];
    @$email = $_SESSION['email'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Profile</title>
<style type="text/css">
body {
  margin-left: 0px;
  margin-top: 0px;
  margin-right: 0px;
  margin-bottom: 0px;
  background-image: url(../images/bg.gif);
}
</style>
<link href="../style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">
<?php include 'header.php'; ?>
  <div id="content_panel">
    <div id="heading">Home<hr size="2" color="#FFFFFF" ice:repeating=""/></div>
    <div id="form">
    <form method="post" action="approve_reject_db.php">
    <fieldset>
    <legend>Personal Information</legend>
    <label for="staff_id"><span>Staff ID </span>
      <input type="text" name="staff_id" id="staff_id" readonly="true" value="<?php echo $staff_id ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Staff Name </span>
      <input type="text" readonly="true" value="<?php echo $FirstName." ".$LastName ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Email </span>
      <input type="text" readonly="true" value="<?php echo $email ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Gender </span>
      <input type="text" readonly="true" value="<?php echo $Gender ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Phone Number </span>
      <input type="text" readonly="true" value="<?php echo $PhoneNumber ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Department </span>
      <input type="text" readonly="true" value="<?php echo $Dpt?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    <label for="staff_name"><span>Designation </span>
      <input type="text" readonly="true" value="<?php echo $Des ?>" style="background-color:#F6F6F6; color:#069" />
    </label>
    </fieldset>
    <br />
    </form>
    </div>
  </div>
<?php include 'sidebar.php'; ?>
<?php include 'footer.php'; ?>
</div>
</div>
</body>
</html>
<?php
  }
  else
  {
    header("Location: ../index.html");
  }
  @mysql_close($connection);
?>